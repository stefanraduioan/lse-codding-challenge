﻿// See https://aka.ms/new-console-template for more information

using System.Globalization;
using LseCodingChallenge.Services.Implementation;

Console.WriteLine("Welcome to LSE coding challenge!!");
Console.WriteLine();

var isValueValid = false;
var value = 0;
while (!isValueValid)
{
    Console.WriteLine("Please enter the number of sample files from each exchange(1 or 2): ");
    var isValid = int.TryParse(Console.ReadLine(), out value);

    if (!isValid || value is not (1 or 2))
    {
        Console.WriteLine($"Value {value} is not valid. Please try again.");
        continue;
    }

    isValueValid = true;
}

var factory = new ForecastServiceFactory();
var predictor = factory.Create(value);

Console.WriteLine("Forecast Service is initialized...");

while (true)
{
    Console.WriteLine("Please enter timestamp(format dd-MM-yyyy) or 'EXIT'");
    var key = Console.ReadLine();

    if (key == "EXIT")
    {
        Console.WriteLine("Closing program...");
        break;
    }

    var isValid = DateTime.TryParseExact(
        key, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out var timestamp);

    if (!isValid)
    {
        Console.WriteLine("Value is not valid. Please try again.");
        continue;
    }

    var isSuccess = await predictor.ForecastAsync(timestamp);

    if (isSuccess)
    {
        Console.WriteLine("Success! Check output folder for results.");
    }
}