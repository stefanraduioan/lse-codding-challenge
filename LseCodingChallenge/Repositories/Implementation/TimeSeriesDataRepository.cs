﻿using System.Collections.Immutable;
using System.Globalization;
using System.Text;
using LseCodingChallenge.Models;
using LseCodingChallenge.Services;

namespace LseCodingChallenge.Repositories.Implementation;

public class TimeSeriesDataRepository : ITimeSeriesDataRepository
{
    public async Task<IReadOnlyCollection<TimeSeriesData>?> GetAsync(DateTime afterTimestamp,
        int maxNumberOfDataPoints = 10)
    {
        try
        {
            var filePaths = DetermineFilePaths(DatabaseConfiguration.DATABASE_INPUT_PATH);

            var timeSeriesDataList = await FetchTimeSeriesDataAsync(filePaths);

            var result = FilterDataPoints(timeSeriesDataList, afterTimestamp, maxNumberOfDataPoints);

            return result;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Failed to retrieve data! Reason: {ex.Message}.");

            return null;
        }
    }

    public async Task<bool> SaveAsync(TimeSeriesData timeSeriesData)
    {
        try
        {
            var fileName = $"{timeSeriesData.FileName}.csv";
            var filePath = Path.Combine(DatabaseConfiguration.DATABASE_OUTPUT_PATH, timeSeriesData.ExchangeId);

            var fileContent = timeSeriesData.DataPoints
                .Select(dataPoint =>
                    $"{dataPoint.StockId},{dataPoint.Timestamp:dd-MM-yyyy},{dataPoint.StockPrice}{Environment.NewLine}")
                .Aggregate(string.Empty, (current, row) => current + row);

            Directory.CreateDirectory(filePath);

            await File.WriteAllTextAsync(Path.Combine(filePath, fileName), fileContent);

            return true;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occured while saving time series data!! Reason {ex.Message}");
            return false;
        }
    }

    private static IReadOnlyCollection<TimeSeriesData> FilterDataPoints(
        IReadOnlyCollection<TimeSeriesData> timeSeriesDataList, DateTime afterTimestamp, int maxNumberOfDataPoints)
    {
        var result = new List<TimeSeriesData>();

        foreach (var timeSeriesData in timeSeriesDataList)
        {
            var filteredDataPoints = timeSeriesData.DataPoints
                .OrderBy(x => x.Timestamp)
                .Where(x => x.Timestamp >= afterTimestamp)
                .Take(maxNumberOfDataPoints);

            result.Add(new TimeSeriesData()
            {
                ExchangeId = timeSeriesData.ExchangeId,
                FileName = timeSeriesData.FileName,
                DataPoints = filteredDataPoints.ToImmutableList()
            });
        }

        return result;
    }

    private static async Task<IReadOnlyCollection<TimeSeriesData>> FetchTimeSeriesDataAsync(
        IEnumerable<string> filePaths)
    {
        var result = new List<TimeSeriesData>();

        foreach (var filePath in filePaths)
        {
            var fileContent = await File.ReadAllLinesAsync(filePath);

            var dataPoints = ProcessDataPoints(fileContent);

            var timeSeriesData = CreateTimeSeriesData(filePath, dataPoints);

            result.Add(timeSeriesData);
        }

        return result;
    }

    private static TimeSeriesData CreateTimeSeriesData(string filePath, IReadOnlyCollection<DataPoint> dataPoints)
    {
        var parts = filePath.Split('\\');
        var fileName = parts[^1].Split('.').First();
        var exchangeId = parts[^2];

        return new TimeSeriesData()
        {
            ExchangeId = exchangeId,
            FileName = fileName,
            DataPoints = dataPoints
        };
    }

    private static IReadOnlyCollection<DataPoint> ProcessDataPoints(IEnumerable<string> fileContent)
    {
        var dataPoints = new List<DataPoint>();

        foreach (var row in fileContent)
        {
            if (row is null or "")
            {
                continue;
            }

            var values = row.Split(',');

            if (values.Length != 3)
            {
                Console.WriteLine("Warning! Incorrect number of values. Ignoring entry!");
                continue;
            }

            dataPoints.Add(new DataPoint()
            {
                StockId = values[0],
                Timestamp = DateTime.ParseExact(values[1], "dd-MM-yyyy", CultureInfo.InvariantCulture),
                StockPrice = decimal.Parse(values[2])
            });
        }

        return dataPoints;
    }

    private static IReadOnlyCollection<string> DetermineFilePaths(string directory)
    {
        var filePaths = new List<string>();

        try
        {
            filePaths.AddRange(Directory.GetFiles(directory, "*.csv"));

            foreach (var subDirectory in Directory.GetDirectories(directory))
            {
                filePaths.AddRange(DetermineFilePaths(subDirectory));
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Failed to get filePaths.Reason: {ex.Message}");
            throw;
        }

        return filePaths;
    }
}