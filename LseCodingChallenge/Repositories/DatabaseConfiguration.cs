﻿using System.IO;

namespace LseCodingChallenge.Repositories;

public static class DatabaseConfiguration
{
    public static string DATABASE_INPUT_PATH =>
        Directory.GetCurrentDirectory() + "\\Repositories\\Database\\Input\\";

    public static string DATABASE_OUTPUT_PATH =>
        Directory.GetCurrentDirectory() + "\\Repositories\\Database\\Output\\";
}