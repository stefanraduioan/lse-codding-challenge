﻿namespace LseCodingChallenge.Models;

public class StockPricePredictorOptions
{
    public int NumberOfSampleFiles { get; }

    public int NumberOfPredictedValues { get; }

    public StockPricePredictorOptions(int numberOfSampleFiles)
    {
        NumberOfSampleFiles = numberOfSampleFiles;
        NumberOfPredictedValues = 3;
    }
}