﻿using System.Collections.Generic;

namespace LseCodingChallenge.Models;

public record TimeSeriesData
{
    public string ExchangeId { get; init; }

    public string FileName { get; init; }

    public IReadOnlyCollection<DataPoint> DataPoints { get; init; } = new List<DataPoint>();
}