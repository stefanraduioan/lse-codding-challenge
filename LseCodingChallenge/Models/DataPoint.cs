﻿using System;

namespace LseCodingChallenge.Models;

public record DataPoint
{
    public string StockId { get; init; }

    public DateTime Timestamp { get; init; }

    public decimal StockPrice { get; init; }
}