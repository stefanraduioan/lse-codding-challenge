﻿using LseCodingChallenge.Models;

namespace LseCodingChallenge.Services;

public interface ITimeSeriesDataRepository
{
    /// <summary>
    /// Retrieves a collection of TimeSeriesData
    /// </summary>
    /// <param name="afterTimestamp">From date for DataPoints</param>
    /// <param name="maxNumberOfDataPoints">Maximum number of DataPoints</param>
    /// <returns>Collection of TimeSeriesData</returns>
    public Task<IReadOnlyCollection<TimeSeriesData>?> GetAsync(
        DateTime afterTimestamp, int maxNumberOfDataPoints = 10);

    /// <summary>
    /// Saves a TimeSeriesData
    /// </summary>
    /// <param name="timeSeriesData">TimeSeriesData object to be save</param>
    /// <returns>Boolean representing success status</returns>
    public Task<bool> SaveAsync(TimeSeriesData timeSeriesData);
}