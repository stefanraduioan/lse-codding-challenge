﻿using System.Collections.Immutable;
using LseCodingChallenge.Models;

namespace LseCodingChallenge.Services.Implementation;

public class ForecastService : IForecastService
{
    private readonly ITimeSeriesDataFacade _timeSeriesDataFacade;

    private readonly StockPricePredictorOptions _options;

    public ForecastService(ITimeSeriesDataFacade timeSeriesDataFacade, StockPricePredictorOptions options)
    {
        _timeSeriesDataFacade = timeSeriesDataFacade;
        _options = options;
    }

    public async Task<bool> ForecastAsync(DateTime timestamp)
    {
        try
        {
            var timeSeriesDataList = await _timeSeriesDataFacade.GetTimeSeriesDataAsync(timestamp);
            var filteredDataList = FilterTimeSeriesData(timeSeriesDataList, _options);

            var isSuccess = await _timeSeriesDataFacade.PredictNextValuesAsync(
                filteredDataList, _options.NumberOfPredictedValues);

            return isSuccess;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Something went wrong: {ex.Message}");
            return false;
        }
    }

    private static IReadOnlyCollection<TimeSeriesData> FilterTimeSeriesData(
        IReadOnlyCollection<TimeSeriesData> timeSeriesData, StockPricePredictorOptions options)
    {
        var filteredGroups =
            from element in timeSeriesData
            group element by element.ExchangeId
            into groups
            select groups.Take(options.NumberOfSampleFiles);

        return filteredGroups.SelectMany(x => x).ToImmutableList();
    }
}