﻿using System.Collections.Immutable;
using LseCodingChallenge.Models;

namespace LseCodingChallenge.Services.Implementation;

public class TimeSeriesPredictor : ITimeSeriesDataPredictor
{
    public TimeSeriesData? Predict(TimeSeriesData timeSeriesData, int numberOfPredictions)
    {
        var strategy = DetermineStrategy(numberOfPredictions);

        if (strategy == null)
        {
            Console.WriteLine("Error! No strategy fits request!!!");
            Console.WriteLine("Best prediction is no prediction.");

            return null;
        }

        var starTimestamp = timeSeriesData.DataPoints.First().Timestamp.ToString("dd-MM-yyyy");
        var resultTimestamp = timeSeriesData.FileName + $"_Prediction_{starTimestamp}";

        var nextDataPoints = strategy.Apply(timeSeriesData.DataPoints);
        var resultDataPoints = timeSeriesData.DataPoints.Concat(nextDataPoints).ToImmutableList();

        return new TimeSeriesData()
        {
            ExchangeId = timeSeriesData.ExchangeId,
            FileName = resultTimestamp,
            DataPoints = resultDataPoints
        };
    }

    private static IPredictionStrategy? DetermineStrategy(int numberOfPredictions)
    {
        return numberOfPredictions == 3 ? new ExamplePredictionStrategy() : null;
    }
}