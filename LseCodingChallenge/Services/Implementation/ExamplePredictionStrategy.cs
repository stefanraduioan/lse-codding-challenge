﻿using LseCodingChallenge.Models;

namespace LseCodingChallenge.Services.Implementation;

public class ExamplePredictionStrategy : IPredictionStrategy
{
    private const int NUMBER_OF_RESULTS = 3;

    public IReadOnlyCollection<DataPoint> Apply(IReadOnlyCollection<DataPoint> dataPoints)
    {
        var result = new List<DataPoint>();

        try
        {
            var stockId = dataPoints.First().StockId;
            var predictionPrices = new List<decimal>();
            var lastTimestamp = dataPoints.OrderByDescending(x => x.Timestamp).First().Timestamp;

            // first predicted (n+1) data point is same as the 2nd highest value present in the 10 data points
            var firstStockPrice = dataPoints.OrderByDescending(x => x.StockPrice).Skip(1).First().StockPrice;
            predictionPrices.Add(firstStockPrice);

            // n+2 data point has half the difference between n and n +1
            var secondStockPrice = Math.Abs(dataPoints.Last().StockPrice - firstStockPrice) / 2;
            predictionPrices.Add(secondStockPrice);

            // n+3 data point has 1/4th the difference between n+1 and n+2
            var thirdStockPrice = Math.Abs(secondStockPrice - firstStockPrice) / 4;
            predictionPrices.Add(thirdStockPrice);

            for (var i = 0; i < NUMBER_OF_RESULTS; i++)
            {
                result.Add(new DataPoint()
                {
                    StockId = stockId,
                    Timestamp = lastTimestamp.AddDays(i + 1),
                    StockPrice = predictionPrices.Skip(i).First()
                });
            }
        }
        catch (Exception ex)
        {
            var message = $"An error occured while applying {nameof(ExamplePredictionStrategy)}. Reason: {ex.Message}";
            Console.WriteLine(message);
            throw;
        }

        return result;
    }
}