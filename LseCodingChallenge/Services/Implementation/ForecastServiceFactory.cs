﻿using System;
using LseCodingChallenge.Models;
using LseCodingChallenge.Repositories.Implementation;

namespace LseCodingChallenge.Services.Implementation;

public class ForecastServiceFactory : IForecastServiceFactory
{
    public IForecastService Create(int numberOfSampleFiles)
    {
        if (numberOfSampleFiles is not (1 or 2))
        {
            var message =
                $"{nameof(ForecastServiceFactory)}: Failed to {nameof(Create)} instance of {nameof(IForecastService)}." +
                $" Reason: {numberOfSampleFiles} is not 1 or 2.";

            Console.WriteLine(message);

            throw new InvalidOperationException(message);
        }

        var options = new StockPricePredictorOptions(numberOfSampleFiles);

        ITimeSeriesDataRepository repository = new TimeSeriesDataRepository();
        ITimeSeriesDataPredictor predictor = new TimeSeriesPredictor();
        ITimeSeriesDataFacade facade = new TimeSeriesDataFacade(repository, predictor);

        return new ForecastService(facade, options);
    }
}