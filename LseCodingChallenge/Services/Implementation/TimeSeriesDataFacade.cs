﻿using LseCodingChallenge.Models;

namespace LseCodingChallenge.Services.Implementation;

public class TimeSeriesDataFacade : ITimeSeriesDataFacade
{
    private readonly ITimeSeriesDataRepository _repository;
    private readonly ITimeSeriesDataPredictor _predictor;

    public TimeSeriesDataFacade(
        ITimeSeriesDataRepository repository,
        ITimeSeriesDataPredictor predictor)
    {
        _repository = repository;
        _predictor = predictor;
    }

    public async Task<IReadOnlyCollection<TimeSeriesData>> GetTimeSeriesDataAsync(DateTime afterTimestamp)
    {
        var timeSeriesDataList = await _repository.GetAsync(afterTimestamp);

        if (timeSeriesDataList == null)
        {
            const string message = "An error occured then retrieving data!";
            Console.WriteLine(message);

            throw new InvalidOperationException(message);
        }

        return timeSeriesDataList;
    }

    public async Task<bool> PredictNextValuesAsync(
        IReadOnlyCollection<TimeSeriesData> timeSeriesDataList, int numberOfPredictedValues)
    {
        var isSuccess = false;

        foreach (var timeSeriesData in timeSeriesDataList)
        {
            try
            {
                var prediction = _predictor.Predict(timeSeriesData, numberOfPredictedValues);

                if (prediction == null)
                {
                    Console.WriteLine("Error! Failed to calculate prediction.");
                    continue;
                }

                var isSaved = await _repository.SaveAsync(prediction);

                if (!isSaved)
                {
                    Console.WriteLine("Error! Failed to save prediction.");
                    continue;
                }

                // return success if at list one prediction is successful 
                isSuccess = true;
            }
            catch (Exception ex)
            {
                var message =
                    $"Failed to process Time Series Data {timeSeriesData.ExchangeId}/{timeSeriesData.FileName}. Reason: {ex.Message}.";
                Console.WriteLine(message);
            }
        }

        return isSuccess;
    }
}