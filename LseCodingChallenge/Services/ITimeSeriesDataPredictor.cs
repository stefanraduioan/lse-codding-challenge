﻿using LseCodingChallenge.Models;

namespace LseCodingChallenge.Services;

public interface ITimeSeriesDataPredictor
{
    /// <summary>
    /// Creates a TimeSeriesData representing a prediction 
    /// </summary>
    /// <param name="timeSeriesData">Input TimeSeriesData to be used for prediction</param>
    /// <param name="numberOfPredictions">Number of predicted DataPoints</param>
    /// <returns>TimeSeriesData representing a prediction</returns>
    public TimeSeriesData? Predict(TimeSeriesData timeSeriesData, int numberOfPredictions);
}