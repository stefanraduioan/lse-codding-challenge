﻿using LseCodingChallenge.Models;

namespace LseCodingChallenge.Services;

public interface IPredictionStrategy
{
    /// <summary>
    /// Prediction Strategy that calculates next DataPoints for a given series
    /// </summary>
    /// <param name="dataPoints">DataPoints to be used for prediction</param>
    /// <returns>Predicted DataPoints</returns>
    public IReadOnlyCollection<DataPoint> Apply(IReadOnlyCollection<DataPoint> dataPoints);
}