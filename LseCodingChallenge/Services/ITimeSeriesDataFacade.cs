﻿using LseCodingChallenge.Models;

namespace LseCodingChallenge.Services;

public interface ITimeSeriesDataFacade
{
    /// <summary>
    /// Function that, for each file provided, returns 10 consecutive data points starting from a timestamp
    /// </summary>
    /// <param name="afterTimestamp">Date for staring Time Series Data</param>
    /// <returns>A TimeSeriesData object</returns>
    public Task<IReadOnlyCollection<TimeSeriesData>> GetTimeSeriesDataAsync(DateTime afterTimestamp);

    /// <summary>
    /// Function that gets the output from 1st one and predicts the next values in the timeseries data
    /// </summary>
    /// <param name="timeSeriesDataList">A list of TimeSeriesData</param>
    /// <param name="numberOfPredictedValues">How many values to predict (default 3)</param>
    /// <returns>A list of TimeSeriesData which represents predictions </returns>
    public Task<bool> PredictNextValuesAsync(
        IReadOnlyCollection<TimeSeriesData> timeSeriesDataList, int numberOfPredictedValues = 3);
}