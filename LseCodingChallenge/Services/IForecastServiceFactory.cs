﻿namespace LseCodingChallenge.Services;

public interface IForecastServiceFactory
{
    /// <summary>
    /// Factory service to for creating a ForecastService
    /// </summary>
    /// <param name="numberOfSampleFiles">Number of files to be used for each exchange directory.</param>
    /// <returns>A ForecastService instance.</returns>
    IForecastService Create(int numberOfSampleFiles);
}