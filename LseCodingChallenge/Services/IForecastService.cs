﻿namespace LseCodingChallenge.Services;

public interface IForecastService
{
    /// <summary>
    /// Generates next DataPoints for available Time Series for an input date
    /// </summary>
    /// <param name="timestamp">From date for date points used in forecasting.</param>
    /// <returns>Returns boolean value representing success status.</returns>
    public Task<bool> ForecastAsync(DateTime timestamp);
}