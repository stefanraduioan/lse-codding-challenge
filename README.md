# lse-codding-challenge


## How-To Build Executable (CLI)

### Pre-requisites
Download latest version of the .net 6 SDK. 

### Steps
Open a terminal (e.g: bash, command prompt, powershell) and in the same directory as your .csproj file enter the below command:
```
dotnet publish --output "{any directory}" --runtime {runtime}
  --configuration {Debug|Release} -p:PublishSingleFile={true|false}
  -p:PublishTrimmed={true|false} --self-contained {true|false}
```
example:
```
dotnet publish --output "c:/temp/myapp" --runtime win-x64 --configuration Release
  -p:PublishSingleFile=true -p:PublishTrimmed=true --self-contained true
```

## Using Tool

Find executable in published directory and double click to open it. Follow instructions prompt in terminal.

Tool output files will be persited in "Output" directory with the following path "...\Repositories\Database\Output" relative to the executable app.